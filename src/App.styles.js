import { createGlobalStyle } from "styled-components";

export const
    darkGrey = '#BDBDBD',
    blueHover = '#154BAD',
    blueMain = '#003594',
    blueMain30 = 'rgba(0, 53, 148, 0.3)',
    blueMain10 = 'rgba(0, 53, 148, 0.1)',
    bluePressed = '#032F7C',
    dark = 'rgb(17, 24, 32)',
    dark10 = 'rgba(17,24,32,0.1)',
    dark30 = 'rgba(17,24,32,0.3)',
    dark50 = 'rgba(17,24,32,0.5)',
    white = '#FFFFFF',
    white50 = 'rgba(255, 255, 255, 0.5)',
    lightGrey = '#F6F8FC',
    lightBlue = '#7CA3DC',
    gray = '#EBEEF3',
    breakpointMobile = '768px';
;

export const GlobalStyles = createGlobalStyle`
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
`;