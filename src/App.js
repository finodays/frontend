/** Components */
import ChatWindow from './containers/ChatWindow';

export default function App() {
    return (
        <div className="App">
            <ChatWindow />
        </div>
    );
}
