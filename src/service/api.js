import config from "../config";

class Service {
    /**
     * Отправка сообщения
     * @param text - текст сообщения
     */
    async sendMessage(text) {
        const url = `${config.host}/api/getTextEmotionalScore`;

        const response = await fetch(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                text: text
            }),
        });

        const jsonData = await response.json();

        if (jsonData?.statusCode) {
            throw new Error(response.message);
        }

        return jsonData.emotionalScore;
    }
}

export default new Service();
