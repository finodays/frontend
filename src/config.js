const config = {
    msgTypes: {
        client: 'clientMessage',
        bot: 'botMessage',
    },
    host: 'http://localhost:3030',
    emotionalScore: {
        positive: 'POSITIVE',
        negative: 'NEGATIVE',
        neutral: 'NEUTRAL',
    },
};

export default config;
