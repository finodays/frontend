import styled from 'styled-components';
import { blueMain, breakpointMobile, lightBlue, lightGrey } from '../../App.styles';

export const FooterWrapper = styled.div`
    position: absolute;
    bottom: 0px;
    background: white;
    padding: 16px;
    width: 100%;
    box-sizing: border-box;
    height: 80px;
`;

export const InputWrapper = styled.div`
    height: 100%;
    width: 100%;
    box-sizing: border-box;
    border: 1px solid ${lightBlue};
    border-radius: 4px;
    padding: 16px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    transition: 0.2s;

    & input {
        outline: 0;
        border: 0px;
        font-size: 14px;
        font-family: Arial;
        width: 80%;
    }

    &:hover {
        border: 1px solid ${blueMain};
    }

`;

export const ChatWindowWrapper = styled.div`
    display: flex;
    flex-direction: column;
    background: ${lightGrey};
    border-radius: 10px;
    height: 604px;
    width: 379px;
    box-shadow: 0px 0px 19px -6px rgba(34, 60, 80, 0.2);
    margin: 64px auto;
    position: relative;
    overflow: hidden;

    @media (max-width: ${breakpointMobile}) {
        width: 100%;
    }
`;

export const HeaderWrapper = styled.header`
    width: 100%;
    height: 80px;
    background: ${blueMain};
    padding: 16px;
    border-radius: 10px 10px 0 0;
    box-sizing: border-box;
    display: flex;
    align-items: center;
`;

export const HeaderTitleWrapper = styled.div`
    margin-left: 16px;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

export const HeaderMainTitle = styled.span`
    font-family: Open Sans;
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    color: white;
`;

export const HeaderSecondaryTitle = styled.span`
    font-family: Open Sans;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    color: white;
`;

export const SendButton = styled.span`
    .wrapper {
        width: 24px;
        height: 24px;
    }
    cursor: pointer;

    & svg path {
        fill: ${blueMain};
        fill-opacity: 1;
        transition: 0.2s;
    }
`;

