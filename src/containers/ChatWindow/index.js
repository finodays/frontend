/** React */
import { useEffect, useRef, useState } from 'react';

/** Styles */
import {
    ChatWindowWrapper,
    FooterWrapper,
    HeaderMainTitle,
    HeaderSecondaryTitle,
    HeaderTitleWrapper,
    HeaderWrapper,
    InputWrapper,
    SendButton,
} from './styles';

/** Media */
import { ReactComponent as HeaderAvatar } from '../../media/header-avatar.svg';
import { ReactComponent as SendIcon } from '../../media/send-icon.svg';

/** Components */
import MessageList from '../../components/MessageList';

/** Config */
import config from '../../config';

/** Service */
import Service from '../../service/api';

export default function ChatWindow() {
    const [messageList, setMessageList] = useState([
        {
            type: config.msgTypes.bot,
            text: 'Здраствуйте! Отправьте сообщение произвольного содержания, чтобы получить анализ эмоциональной окраски его текста',
        },
    ]);
    const [lastEmotionalScore, setLastEmotionalScore] = useState(null);

    const inputRef = useRef();
    const messageListRef = useRef();

    useEffect(() => {
        messageListRef.current.scrollTo({
            top: messageListRef.current.scrollHeight,
            behavior: 'smooth',
        });
    }, [messageList]);

    useEffect(() => {
        if (lastEmotionalScore) {
            switch (lastEmotionalScore) {
                case config.emotionalScore.positive:
                    sendBotMessage(
                        'Cообщение интерпретировано как позитивное, следующим действием можно предложить выставить оценку приложению'
                    );
                    setLastEmotionalScore(null);
                    break;

                case config.emotionalScore.negative:
                    sendBotMessage(
                        'Cообщение интерпретировано как негативное, следующим действием можно подключить пользователя к оператору'
                    );
                    setLastEmotionalScore(null);
                    break;

                case config.emotionalScore.neutral:
                    sendBotMessage(
                        'Сообщение интерпретировано как нейтральное'
                    );
                    setLastEmotionalScore(null);
                    break;

                default:
                    break;
            }
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [lastEmotionalScore]);

    const sendMessage = async () => {
        try {
            if (inputRef.current.value) {
                const emotionalScore = await Service.sendMessage(
                    inputRef.current.value
                );

                setMessageList([
                    ...messageList,
                    {
                        type: config.msgTypes.client,
                        text: inputRef.current.value,
                    },
                ]);
                inputRef.current.value = '';

                setLastEmotionalScore(emotionalScore);
            }
        } catch (err) {
            setMessageList([
                ...messageList,
                {
                    type: config.msgTypes.client,
                    text: inputRef.current.value,
                },
                {
                    type: config.msgTypes.bot,
                    text: 'Алгоритм не смог дать однозначного ответа, попробуйте снова',
                },
            ]);

            inputRef.current.value = '';
        }
    };

    const sendBotMessage = (text) => {
        setMessageList([
            ...messageList,
            {
                type: config.msgTypes.bot,
                text,
            },
        ]);
    };

    const sendMessageWithEnter = (e) => {
        if (e.key === 'Enter') {
            sendMessage();
        }
    };

    return (
        <ChatWindowWrapper>
            <HeaderWrapper>
                <HeaderAvatar />
                <HeaderTitleWrapper>
                    <HeaderMainTitle>Анализ</HeaderMainTitle>
                    <HeaderSecondaryTitle>
                        Эмоциональная окраска
                    </HeaderSecondaryTitle>
                </HeaderTitleWrapper>
            </HeaderWrapper>
            <MessageList messageList={messageList} refValue={messageListRef} />
            <FooterWrapper onKeyDown={sendMessageWithEnter}>
                <InputWrapper>
                    <input
                        type="text"
                        placeholder="Введите ваше сообщение..."
                        autoFocus={true}
                        ref={inputRef}
                    />
                    <SendButton onClick={sendMessage}>
                        <SendIcon />
                    </SendButton>
                </InputWrapper>
            </FooterWrapper>
        </ChatWindowWrapper>
    );
}
