import styled from 'styled-components';
import { gray } from '../../App.styles';

export const BotMessageWrapper = styled.div`
    background: ${gray};
    border-radius: 4px 10px 10px 10px;
    padding: 16px;
    font-family: Open Sans;
    font-size: 14px;
    line-height: 20px;
    margin-bottom: 24px;
    max-width: 80%;
`;

export const ClientMessageWrapper = styled.div`
    margin-bottom: 24px;
    align-self: flex-end;
    max-width: 80%;
    background: white;
    border-radius: 10px 10px 4px 10px;
    padding: 16px;
    font-family: Open Sans;
    font-size: 14px;
    line-height: 20px;
`;