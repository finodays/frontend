/** React */
import { Fragment } from 'react';

/** Config */
import config from '../../config';
import { BotMessageWrapper, ClientMessageWrapper } from './styles';

export default function Message(props) {
    const { type, text } = props;

    return (
        <Fragment>
            {type && type === config.msgTypes.bot && (
                <BotMessageWrapper>{text}</BotMessageWrapper>
            )}
            {type && type === config.msgTypes.client && (
                <ClientMessageWrapper>{text}</ClientMessageWrapper>
            )}
        </Fragment>
    );
}
