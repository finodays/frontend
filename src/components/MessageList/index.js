/** Styles */
import { MessageListWrapper } from './styles';

/** Components */
import Message from '../Message';

/** Misc */
import { v4 } from 'uuid';

export default function MessageList(props) {
    const { messageList, refValue } = props;

    return (
        <MessageListWrapper ref={refValue}>
            {messageList &&
                messageList.map((item) => {
                    return (
                        <Message key={v4()} type={item.type} text={item.text} />
                    );
                })}
        </MessageListWrapper>
    );
}
