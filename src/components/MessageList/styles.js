import styled from 'styled-components';

export const MessageListWrapper = styled.div`
    width: 100%;
    height: 444px;
    display: flex;
    overflow-y: auto;
    flex-direction: column;
    padding: 16px;
    box-sizing: border-box;
`;
